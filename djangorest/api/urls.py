from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import reader_list

urlpatterns = {
    url(r'^readers/', reader_list),
}

urlpatterns = format_suffix_patterns(urlpatterns)
