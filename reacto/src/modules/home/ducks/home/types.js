export const FETCH_ALL = "home/FETCH_ALL";
export const FETCH_ALL_SUCCESS = "home/FETCH_ALL_SUCCESS";
export const FETCH_ALL_ERROR = "home/FETCH_ALL_ERROR";
export const CREATE = "home/CREATE";
export const CREATE_SUCCESS = "home/CREATE_SUCCESS";
export const CREATE_ERROR = "home/CREATE_ERROR";
