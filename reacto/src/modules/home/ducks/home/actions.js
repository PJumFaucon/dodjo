import axios from "axios";
import * as types from "./types";

export const getReaders = () => {
  return dispatch => {
    dispatch({
      type: types.FETCH_ALL
    });
    axios({
      method: "get",
      url: "http://localhost:8000/readers/"
    })
      .then(response => {
        if (response.data) {
          dispatch({
            type: types.FETCH_ALL_SUCCESS,
            payload: response.data
          });
        }
      })
      .catch(err => {
        dispatch({
          type: types.FETCH_ALL_ERROR,
          payload: err
        });
      });
  };
};

export const addReader = reader => {
  return dispatch => {
    dispatch({
      type: types.CREATE
    });
    axios({
      method: "post",
      url: "http://localhost:8000/readers/",
      data: { name: reader }
    })
      .then(response => {
        if (response.data) {
          dispatch({
            type: types.CREATE_SUCCESS,
            payload: response.data
          });
          dispatch(getReaders());
        }
      })
      .catch(err => {
        dispatch({
          type: types.CREATE_ERROR,
          payload: err
        });
      });
  };
};
