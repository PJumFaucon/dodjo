import React from "react";
import Grid from "material-ui/Grid";

import Reader from "./Reader";

class Main extends React.Component {
  render() {
    return (
      <Grid container>
        <Grid item xs={12}>
          <Reader />
        </Grid>
      </Grid>
    );
  }
}

export default Main;
