import { connect } from "react-redux";
import Reader from "./Reader";
import { actions as home } from "modules/home/ducks/home";

const mapStateToProps = store => {
  return {
    readers: store.Home.readers
  };
};

const mapDispatchToProps = {
  addReader: home.addReader
};

export default connect(mapStateToProps, mapDispatchToProps)(Reader);
