import React from "react";
import Typography from "material-ui/Typography";
import Paper from "material-ui/Paper";
import Grid from "material-ui/Grid";
import { withStyles } from "material-ui/styles";
import { lightGreen } from "material-ui/colors";
import { TextField } from "material-ui";
import Button from "material-ui/Button";

const styles = theme => ({
  main: {
    padding: 5
  },
  paperDashboard: {
    padding: 15,
    height: 320
  },
  active: {
    backgroundColor: lightGreen[400]
  }
});

class Reader extends React.Component {
  state = {
    name: ""
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.props.addReader(this.state.name);
  };
  render() {
    const { classes, readers } = this.props;

    return (
      <Paper className={classes.paperDashboard}>
        <Grid container alignItems="center" direction="column">
          <Grid item>
            {readers.map(reader => {
              return (
                <Typography key={reader.id} variant="headline" align="center">
                  {reader.name}
                </Typography>
              );
            })}
          </Grid>
          <Grid item>
            <form className={classes.container} noValidate autoComplete="off">
              <TextField
                id="name"
                label="Name"
                className={classes.textField}
                value={this.state.name}
                onChange={this.handleChange("name")}
                margin="normal"
              />
              <Button onClick={this.handleSubmit}>bim</Button>
            </form>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default withStyles(styles)(Reader);
